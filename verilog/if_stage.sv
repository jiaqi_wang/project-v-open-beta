/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  if_stage.v                                          //
//                                                                     //
//  Description :  instruction fetch (IF) stage of the pipeline;       // 
//                 fetch instruction, compute next PC location, and    //
//                 send them down the pipeline.                        //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

module if_stage(
	input         clock,                  // system clock
	input         reset,                  // system reset

	//branch predictor
	input         branch_take_branch,      // taken-branch signal
	input  [`XLEN-1:0] branch_target_pc,        // branch predictor address

	//I cache
	input  [63:0] Icache2proc_data_PC, Icache2proc_data_PC_plus_4, Icache2proc_data_PC_plus_8,Icache2proc_data_PC_plus_12, // Data coming back from Icache
	output logic [`XLEN-1:0] proc2Icache_addr_PC, proc2Icache_addr_PC_plus_4, proc2Icache_addr_PC_plus_8, proc2Icache_addr_PC_plus_12,   // Address sent to Icache


	output IF_ID_PACKET if_packet_out         // Output data packet from IF going to ID, see sys_defs for signal information 
);


	logic    [`XLEN-1:0] PC_reg;             // PC we are currently fetching
	logic    [`XLEN-1:0] PC_plus_4,PC_plus_8,PC_plus_12,PC_plus_16;
	logic    [`XLEN-1:0] next_PC;
	logic                PC_enable;
	
	//I cache:
	//send address
	assign proc2Icache_addr_PC = {PC_reg[`XLEN-1:3], 3'b0};
	assign proc2Icache_addr_PC_plus_4 = {PC_plus_4[`XLEN-1:3], 3'b0};
	assign proc2Icache_addr_PC_plus_8 = {PC_plus_8[`XLEN-1:3], 3'b0};
	assign proc2Icache_addr_PC_plus_12 = {PC_plus_12[`XLEN-1:3], 3'b0};
	
	//receive data
	assign if_packet_out.inst_PC = PC_reg[2] ? Icache2proc_data_PC[63:32] : Icache2proc_data_PC[31:0];
	assign if_packet_out.inst_PC_plus_4 = PC_plus_4[2] ? Icache2proc_data_PC_plus_4[63:32] : Icache2proc_data_PC_plus_4[31:0];
	assign if_packet_out.inst_PC_plus_8 = PC_plus_8[2] ? Icache2proc_data_PC_plus_8[63:32] : Icache2proc_data_PC_plus_8[31:0];
	assign if_packet_out.inst_PC_plus_12 = PC_plus_12[2] ? Icache2proc_data_PC_plus_12[63:32] : Icache2proc_data_PC_plus_12[31:0];
	
	// use PC - PC+12, next PC is PC+16
	assign PC_plus_4 = PC_reg + 4;
	assign PC_plus_8 = PC_reg + 8;
	assign PC_plus_12 = PC_reg + 12;
	assign PC_plus_16 = PC_reg + 16;
	assign if_packet_out.PC_plus_4 = PC_plus_4;
	assign if_packet_out.PC_plus_8 = PC_plus_8;
	assign if_packet_out.PC_plus_12 = PC_plus_12;
	assign if_packet_out.PC_plus_16 = PC_plus_16;
	assign if_packet_out.PC  = PC_reg;

	// taken branch
	assign next_PC = branch_take_branch ? branch_target_pc : PC_plus_16;
	
	// The take-branch signal must override stalling (otherwise it may be lost)
	//??????
	assign PC_enable = if_packet_out.valid | branch_take_branch;

	
	// This register holds the PC value
	always_ff @(posedge clock) begin
		if(reset)
			PC_reg <= `SD 0;       // initial PC value is 0
		else if(PC_enable)
			PC_reg <= `SD next_PC; // transition to next PC
	end  // always
	
	always_ff @(posedge clock) begin
		if (reset)
			if_packet_out.valid <= `SD 1;  
		else
			if_packet_out.valid <= `SD 1;
	end
endmodule  // module if_stage
