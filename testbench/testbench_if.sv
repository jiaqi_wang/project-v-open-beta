`timescale 1ns/100ps
module testbench;
	logic	clock;
	logic	reset;

	logic	branch_take_branch;
	logic	[`XLEN-1:0] branch_target_pc;

	logic  [63:0] Icache2proc_data_PC, Icache2proc_data_PC_plus_4, Icache2proc_data_PC_plus_8,Icache2proc_data_PC_plus_12;
	logic [`XLEN-1:0] proc2Icache_addr_PC, proc2Icache_addr_PC_plus_4, proc2Icache_addr_PC_plus_8, proc2Icache_addr_PC_plus_12;   
	
	logic [100:0] count;

	IF_ID_PACKET if_packet_out;

	if_stage m0 (.clock(clock),.reset(reset),.branch_take_branch(branch_take_branch),.branch_target_pc(branch_target_pc),.Icache2proc_data_PC(Icache2proc_data_PC),.Icache2proc_data_PC_plus_4(Icache2proc_data_PC_plus_4),.Icache2proc_data_PC_plus_8(Icache2proc_data_PC_plus_8),.Icache2proc_data_PC_plus_12(Icache2proc_data_PC_plus_12),.proc2Icache_addr_PC(proc2Icache_addr_PC),.proc2Icache_addr_PC_plus_4(proc2Icache_addr_PC_plus_4),.proc2Icache_addr_PC_plus_8(proc2Icache_addr_PC_plus_8),.proc2Icache_addr_PC_plus_12(proc2Icache_addr_PC_plus_12),.if_packet_out(if_packet_out));

	always begin
		#5 clock=~clock;
		count=count+1;
	end

	always_comb begin
		if (count<5) begin
		Icache2proc_data_PC=0;
		Icache2proc_data_PC_plus_4=0;
		Icache2proc_data_PC_plus_8=0;
		Icache2proc_data_PC_plus_12=0;
		branch_target_pc=64'd123;
	end else begin
		Icache2proc_data_PC={proc2Icache_addr_PC,proc2Icache_addr_PC};
		Icache2proc_data_PC_plus_4={proc2Icache_addr_PC_plus_4,proc2Icache_addr_PC_plus_4};
		Icache2proc_data_PC_plus_8={proc2Icache_addr_PC_plus_8,proc2Icache_addr_PC_plus_8};
		Icache2proc_data_PC_plus_12={proc2Icache_addr_PC_plus_12,proc2Icache_addr_PC_plus_12};
		branch_target_pc=64'd123;
	end
	end

	always @(posedge clock) begin
		$display ("if_packet_out.PC:%d",if_packet_out.PC);
	end

	initial begin
		clock=0;
		count=0;
		reset=1;
		branch_take_branch=0;
		#20
		reset=0;
		#1000
		$finish;
	end
endmodule
